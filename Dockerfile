FROM node:8-alpine
COPY . /
RUN mkdir images
RUN npm install
CMD node index.js
