# Requirements
1. Lokal node installation (tested with v8.5.0)

# Usage
Execute:

1. `npm install`
2. `node index.js`

# Configuration
1. Variables defined in `index.js`
2. `proxy-list.json`