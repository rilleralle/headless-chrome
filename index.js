const proxies = require('./proxy-list');

const puppeteer = require('puppeteer');
const async = require('async');

const url = 'https://www.soundclick.com/html5/v4/player.cfm?songID=13780062';
const listenToMusicInSeconds = 10;
const pageLoadTimeoutInSeconds = 45;
const maxAsyncCalls = 10;
const makeScreenshot = true;

async function callPage(proxy) {
    console.log("Use proxy " + proxy);
    const browser = await puppeteer.launch({
        args: [
            '--proxy-server='+ proxy,
        ]
    });
    try {
        const page = await browser.newPage();
        await page.goto(url, {"timeout": pageLoadTimeoutInSeconds * 1000});
        console.log("Page loaded now listening for " + listenToMusicInSeconds + " seconds with proxy " + proxy);
        await page.click('.fa-play');
        await page.waitFor(listenToMusicInSeconds * 1000);
        if (makeScreenshot) {
            await page.screenshot({path: 'images/' + proxy + '.png'});
        }
    } catch (e) {
        console.log("Error using proxy " + proxy + " : " + e.message);
        browser.close();
    }
    browser.close();

}

function createImagesFolder () {
    var fs = require('fs');
    var dir = './images';

    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
    }
}

function shuffleArray(proxies) {
    for (let i = proxies.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [proxies[i], proxies[j]] = [proxies[j], proxies[i]];
    }
    return proxies;
}

createImagesFolder();
async.eachLimit(shuffleArray(proxies), maxAsyncCalls, callPage);
